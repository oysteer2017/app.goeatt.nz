from django.apps import AppConfig


class GoeattappConfig(AppConfig):
    name = 'goeattapp'
