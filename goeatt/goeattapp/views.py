from django.shortcuts import render,redirect
from django.contrib.auth.decorators import login_required
from goeattapp.models import Vendor
from goeattapp.forms import UserForm, VendorForm, MealForm
from django.contrib.auth import authenticate, login
from goeattapp.models import Meal, Order, Driver

from goeattapp.forms import UserForm, VendorForm, UserFormForEdit
#MealForm
from django.contrib.auth.models import User
import ipdb

# Create your views here.
def home(request):
    # return render(request, 'home.html', {})
    #ipdb.set_trace();
    return redirect (vendor_home)

@login_required(login_url='/vendor/sign-in')
def vendor_home(request):
    # return render(request, 'vendor/home.html',{})
    return redirect(vendor_order);
#    return render(request, 'vendor/base.html',{})

@login_required(login_url='/vendor/sign-in')
def vendor_account(request):
    user_form = UserFormForEdit(instance = request.user)
    vendor_form = VendorForm(instance = request.user.vendor)

    if request.method == "POST":
        user_form = UserFormForEdit(request.POST, instance = request.user)
        vendor_form = VendorForm(request.POST, request.FILES, instance = request.user.vendor)

        if user_form.is_valid() and vendor_form.is_valid():
            user_form.save()
            vendor_form.save()

    return render(request, 'vendor/account.html', {
        "user_form": user_form,
        "vendor_form": vendor_form
    })


    return render(request, 'vendor/account.html',{})

@login_required(login_url='/vendor/sign-in')
def vendor_meal(request):

    meals = Meal.objects.filter(vendor = request.user.vendor).order_by("-id")
    return render(request, 'vendor/meal.html', {"meals": meals})
    #return render(request, 'vendor/meal.html',{})


@login_required(login_url='/vendor/sign-in/')
def vendor_edit_meal(request, meal_id):
    form = MealForm(instance = Meal.objects.get(id = meal_id))

    if request.method == "POST":
        form = MealForm(request.POST, request.FILES, instance = Meal.objects.get(id = meal_id))

        if form.is_valid():
            form.save()
            return redirect(vendor_meal)

    return render(request, 'vendor/edit_meal.html', {
        "form": form
    })


@login_required(login_url='/vendor/sign-in')
def vendor_add_meal(request):
    form = MealForm()

    if request.method == "POST":
        form = MealForm(request.POST, request.FILES)

        if form.is_valid():
            meal = form.save(commit=False)
            meal.vendor = request.user.vendor
            meal.save()
            return redirect(vendor_meal)

    return render(request, 'vendor/add_meal.html', {
        "form": form
    })


# @login_required(login_url='/vendor/sign-in')
# def vendor_order(request):
#     return render(request, 'vendor/order.html',{})



@login_required(login_url='/vendor/sign-in/')
def vendor_order(request):
    if request.method == "POST":
        #ipdb.set_trace();
        order = Order.objects.get(id = request.POST["id"], vendor = request.user.vendor)

        if order.status == Order.WAITINGFORCOMFIRMATION:
            order.status = Order.COOKING
            order.save()
        elif order.status == Order.COOKING:
            order.status = Order.READY
            order.save()
        #     or
        # else:
        #     order.status == Order.COOKING:
        #     order.status = Order.READY
        #     order.save()
        # if order.status == Order.COOKING:
        #     order.status = Order.READY
        #     order.save()

    orders = Order.objects.filter(vendor = request.user.vendor).order_by("-id")
    return render(request, 'vendor/order.html', {"orders": orders})



@login_required(login_url='/vendor/sign-in')
def vendor_report(request):
    return render(request, 'vendor/report.html',{})


def vendor_sign_up(request):
    #ipdb.set_trace();
    user_form = UserForm()
    vendor_form = VendorForm()

    if request.method == 'POST':
        user_form = UserForm(request.POST)
        vendor_form = VendorForm(request.POST, request.FILES)

        if user_form.is_valid() and vendor_form.is_valid():
            new_user = User.objects.create_user(**user_form.cleaned_data)
            new_vendor = vendor_form.save(commit=False)
            new_vendor.user = new_user
            new_vendor.save()

            login(request, authenticate(
                username = user_form.cleaned_data["username"],
                password = user_form.cleaned_data["password"]
            ))

            return redirect(vendor_home)

    return render(request, 'vendor/sign-up.html', {
        'userform': user_form,
        'vendorform': vendor_form
        })
