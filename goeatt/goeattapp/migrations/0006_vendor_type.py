# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2019-03-16 23:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goeattapp', '0005_orderdetails'),
    ]

    operations = [
        migrations.AddField(
            model_name='vendor',
            name='type',
            field=models.CharField(default='Restuarant', max_length=500),
        ),
    ]
