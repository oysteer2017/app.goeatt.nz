# Generated by Django 2.1.7 on 2019-03-25 07:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('goeattapp', '0007_auto_20190316_2354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.IntegerField(choices=[(0, 'Waiting for confirmation'), (1, 'Cooking'), (2, 'Ready'), (3, 'On the way'), (4, 'Delivered')]),
        ),
        migrations.AlterField(
            model_name='vendor',
            name='type',
            field=models.CharField(choices=[('R', 'Restaurant'), ('H', 'Homechef')], default='R', max_length=500),
        ),
    ]
