import json

from django.utils import timezone
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from oauth2_provider.models import AccessToken

from goeattapp.models import Vendor, Meal, Order, OrderDetails
from goeattapp.serializers import VendorSerializer, MealSerializer

##############
# CUSTOMERS
##############
def customer_get_vendors(request):
    vendors = VendorSerializer(
        Vendor.objects.all().order_by("-id"),
        many = True,
        context = {"request": request}
    ).data

    return JsonResponse({"vendors": vendors})

def customer_get_meals(request, vendor_id):
    meals = MealSerializer(
        Meal.objects.filter(vendor_id = vendor_id).order_by("-id"),
        many = True,
        context = {"request": request}
    ).data

    return JsonResponse({"meals": meals})


@csrf_exempt
def customer_add_order(request):
    """
        params:
            access_token
            restaurant_id
            address
            order_details (json format), example:
                [{"meal_id": 1, "quantity": 2},{"meal_id": 2, "quantity": 3}]
            stripe_token

        return:
            {"status": "success"}
    """

    if request.method == "POST":
        # Get token
        access_token = AccessToken.objects.get(token = request.POST.get("access_token"),
            expires__gt = timezone.now())

        # Get profile
        customer = access_token.user.customer
        #return JsonResponse({"status": customer})

        # Get Stripe token
        #stripe_token = request.POST["stripe_token"]

        # Check whether customer has any order that is not delivered
        if Order.objects.filter(customer = customer).exclude(status = Order.DELIVERED):
            return JsonResponse({"status": "failed", "error": "Your last order must be completed."})

        # Check Address
        if not request.POST["address"]:
            return JsonResponse({"status": "failed", "error": "Address is required."})

        # Get Order Details
        order_details = json.loads(request.POST["order_details"])

        order_total = 0
        for meal in order_details:
            order_total += Meal.objects.get(id = meal["meal_id"]).price * meal["quantity"]
        #
        if len(order_details) > 0:

            # Step 1: Create a charge: this will charge customer's card
            # charge = stripe.Charge.create(
            #     amount = order_total * 100, # Amount in cents
            #     currency = "usd",
            #     source = stripe_token,
            #     description = "FoodTasker Order"
            # )
        #
            #if charge.status != "failed":
                # Step 2 - Create an Order
            order = Order.objects.create(
                customer = customer,
                vendor_id = request.POST["restaurant_id"],
                total = order_total,
                status = Order.COOKING,
                address = request.POST["address"]
            )
    #
            # Step 3 - Create Order details
            for meal in order_details:
                OrderDetails.objects.create(
                    order = order,
                    meal_id = meal["meal_id"],
                    quantity = meal["quantity"],
                    sub_total = Meal.objects.get(id = meal["meal_id"]).price * meal["quantity"]
                )
        #
            return JsonResponse({"status": "success"})
        #     else:
        #        return JsonResponse({"status": "failed", "error": "Failed connect to Stripe."})
    #return JsonResponse({})

#
# def customer_get_latest_order(request):
#     return JsonResponse({})
